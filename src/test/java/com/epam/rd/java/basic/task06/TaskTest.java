package com.epam.rd.java.basic.task06;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.net.*;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import com.epam.rd.java.basic.TestHelper;

public class TaskTest {

//	private static final String[] TEST_INPUT_DATA_FILES = new String[] {"data.txt", "data2.txt"};

	private static final Class<?> currentClass = MethodHandles.lookup().lookupClass();
	
	private static final String PACKAGE_NAME = currentClass.getPackageName();
	
	private static final PrintStream STD_OUT = System.out;

	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

	private final PrintStream out = new PrintStream(baos);
	
	@BeforeAll
	static void createInputTestDataFiles() throws IOException, URISyntaxException {
		String packagePath = PACKAGE_NAME.replace('.', File.separatorChar);
		URL url = currentClass.getClassLoader().getResource(packagePath + '/' + "data.csv");

		List<String> inputTestDataFiles = Files.readAllLines(Paths.get(url.toURI()))
			.stream()
			.filter(line -> !line.trim().isEmpty())
			.map(line -> line.substring(line.indexOf(';') + 1))
			.map(line -> line.substring(0, line.indexOf(';')))
			.map(String::trim)
			.distinct()
			.collect(Collectors.toList());

		for (String fileName : inputTestDataFiles) {
			InputStream is = currentClass.getClassLoader()
					.getResourceAsStream(packagePath + File.separatorChar + fileName);
			Files.copy(is, Paths.get(fileName), StandardCopyOption.REPLACE_EXISTING);
		}
	}
	
	@BeforeEach
	void setUpControlledOut() {
		System.setOut(out);
	}

	@AfterEach void tearDownControlledOut() {
		System.setOut(STD_OUT);
	}

	@ParameterizedTest
	@CsvFileSource(resources = "data.csv", delimiter = ';')
	void test(String taskName, String dataFileName, String kAsString, String expectedOutput) 
				throws Exception {
		String expected = expectedOutput.replaceAll("~", System.lineSeparator());
		
		Class<?> c = Class.forName(PACKAGE_NAME + '.' + taskName);
		Method task = c.getMethod("main", String[].class);

		System.out.println();
		task.invoke(null, new Object[] {new String[] { dataFileName, kAsString}});

		out.flush();
		String actual = baos.toString();

		TestHelper.logToStdErr(expected, actual);
		assertEquals(expected, actual);
	}

}
