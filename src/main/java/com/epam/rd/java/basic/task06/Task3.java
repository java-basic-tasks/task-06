package com.epam.rd.java.basic.task06;

import java.util.*;
import java.util.regex.*;

public class Task3 {
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Must be at least two parameters (fileName, k)");
			return;
		}

		String fileName = args[0];
		int k = Integer.parseInt(args[1]);
		String content = Task.load(fileName);

		Matcher m = Pattern.compile("(?s)(\\b\\w+\\b)(?=.+\\b\\1\\b)").matcher(content);

		List<String> res = new ArrayList<>();
		int j = 0;

		while (j != k && m.find()) {
			String word = m.group();
			if (!res.contains(word)) {
				res.add(word);
				j++;
			}
		}
		for (String el : res) {
			System.out.println(inverse(el).toUpperCase(Locale.ENGLISH));
		}
		
	}

	private static String inverse(String s) {
		return new StringBuilder(s).reverse().toString();
	}

}
