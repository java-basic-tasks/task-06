package com.epam.rd.java.basic.task06;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.*;
import java.util.Scanner;

public class TaskDataGenerator {
	
	private static final String PACKAGE_NAME = MethodHandles.lookup().lookupClass().getPackageName();

	private static final PrintStream STD_OUT = System.out;

	public static void main(String[] args) throws Exception {
		System.setErr(new PrintStream(OutputStream.nullOutputStream()));
		print("data.txt", 3);
		print("data.txt", 4);
		System.out.println("~~~");
		print("data2.txt", 3);
		print("data2.txt", 4);
		System.out.println("~~~");
		print("data3.txt", 3);
		print("data3.txt", 5);
	}

	public static void print(String fileName, int k) throws Exception {
		// Task1;data.txt;3	;whale ==> 3~cheetah ==> 4~bison ==> 3~

		for (int t = 1; t <= 3; t++) {
			Class<?> c = Class.forName(PACKAGE_NAME + ".Task" + t);
				
			System.err.printf("------- Task%d -------%n", t);
			Method m = c.getMethod("main", String[].class);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintStream out = new PrintStream(baos);
		
			System.setOut(out);
			m.invoke(null, new Object[] {new String[] { fileName, String.valueOf(k) }});

			out.flush();
			String actual = baos.toString();
			System.setOut(STD_OUT);
			
			System.out.print("Task" + t);
			System.out.print(';');

			System.out.print(fileName);
			System.out.print(';');

			System.out.print(k);
			System.out.print(';');
			System.out.print('~');
			
			System.out.println(actual.replaceAll(System.lineSeparator(), "~"));
			
		}

	}

	public static String load(String fileName) {
		StringBuilder sb = new StringBuilder();
		try (Scanner s = new Scanner(new File(fileName))) {
			while (s.hasNextLine()) {
				sb.append(s.nextLine()).append(System.lineSeparator());
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

}
