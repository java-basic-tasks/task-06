package com.epam.rd.java.basic.task06;

import java.io.*;
import java.util.Scanner;

public class Task {
	
	public static void main(String[] args) {
		System.out.println("------- Task1 -------");
		Task1.main(new String[] { "data.txt", "3" });

		System.out.println("------- Task2 -------");
		Task2.main(new String[] { "data.txt", "3" });

		System.out.println("------- Task3 -------");
		Task3.main(new String[] { "data.txt", "3" });
	}

	public static String load(String fileName) {
		StringBuilder sb = new StringBuilder();
		try (Scanner s = new Scanner(new File(fileName))) {
			while (s.hasNextLine()) {
				sb.append(s.nextLine()).append(System.lineSeparator());
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

}
