package com.epam.rd.java.basic.task06;

import java.util.*;
import java.util.regex.*;

public class Task2 {
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Must be at least two parameters (fileName, k)");
			return;
		}

		String fileName = args[0];
		int k = Integer.parseInt(args[1]);
		String content = Task.load(fileName);
		
		Matcher m = Pattern.compile("(?siu)[a-z]+").matcher(content);
		
		Set<String> words = new LinkedHashSet<>();
		while (m.find()) {
			words.add(m.group()); 
		}

		words.stream()
				.sorted((w, w2) -> -w.length() + w2.length())
				.limit(k)
				.forEach(w -> System.out.printf("%s ==> %d%n", w, w.length()));

	}

}
