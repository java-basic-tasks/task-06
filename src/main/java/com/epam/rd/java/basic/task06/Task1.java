package com.epam.rd.java.basic.task06;

import java.util.*;
import java.util.regex.*;

public class Task1 {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Must be at least two parameters (fileName, k)");
			return;
		}

		String fileName = args[0];
		int k = Integer.parseInt(args[1]);
		String content = Task.load(fileName);

		Matcher m = Pattern.compile("(?siu)[a-z]+")
			.matcher(content);

		Map<String, Integer> map = new LinkedHashMap<>();

		while (m.find()) {
			String s = m.group();
			Integer freq = map.get(s);
			map.put(s, (freq == null) ? 1 : (freq + 1));
		}

		map.entrySet()
			.stream()
			.sorted((p, p2) -> -p.getValue() + p2.getValue())
			.limit(k)
			.sorted((p, p2) -> 
				-p.getKey().compareTo(p2.getKey()))
			.forEach(p -> 
				System.out.printf("%s ==> %s%n", p.getKey(), p.getValue()));

	}

}
